Picasa Slideshow Input Filter
=============================

This module provides an input filter to add a Picasa Webalbum Slideshow anywhere input filters are accepted.

To add a slideshow from Picasa Webalbum you simply add the following code to your input field:
 
    [picasa_slideshow id=xxxx user=yyy] 
    
where id=xxxx is the album id from Picasa Webalbum and yyy is optional user id if different from default user id. 

How to find the album id?
-------------------------

* Navigate to the album, you want to embed
* Click on the RSS icon in the right sidebar
* Look in the address bar and find the number behind the albumid
